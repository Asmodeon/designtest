﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZones : MonoBehaviour
{
    public GameObject player;
    private int killcount;
    public GameObject spawn;
    private Vector3 spawnlocation;
    // Start is called before the first frame update
    void Start()
    {
        spawnlocation = spawn.GetComponent<Transform>().position;    
    }

    private void OnTriggerEnter(Collider other)
    {
        killcount += 1;
        player.transform.position = spawnlocation;

    }

    private void OnGUI()
    {
        GUI.Label(new Rect(10, 30, 100, 20), "Death : " + killcount);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
