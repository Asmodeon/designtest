﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinningConditions : MonoBehaviour
{
    public GameObject text;
    // Start is called before the first frame update
    void Start()
    {
        text.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        text.gameObject.SetActive(true);
        Time.timeScale = 0;
    }
}
