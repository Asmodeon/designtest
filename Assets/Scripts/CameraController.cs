﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject pointA;
    public GameObject pointB;
    public GameObject newcamera;
    public GameObject player;
    public float playervelocity;
    private float distance;
    private float movement;
    // Start is called before the first frame update
    void Start()
    {
        newcamera.transform.position = pointA.GetComponent<Transform>().position;
        distance = player.GetComponent<Transform>().position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("d"))
        {
            newcamera.transform.position = Vector3.MoveTowards(transform.position, pointB.GetComponent<Transform>().position, -distance);
        }

    }
}
