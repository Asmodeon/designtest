﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CollectibleScore : MonoBehaviour
{
    public GameObject CoinParticle;


    void OnTriggerEnter(Collider other)
    {
        other.GetComponent<PointCounter>().points++;
        Instantiate(CoinParticle, other.transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
