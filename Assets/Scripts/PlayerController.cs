﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    PlayerController playerController;

    public float speed = 20f;
    public float jumpSpeed;
    //public float disttoGround = 0.5f;
    

    public Rigidbody rb;
    private  Vector3 moveVelocity;
    private Vector3 jumpVelocity;

    public bool isGrounded = false;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
        Vector3 moveInput = new Vector3(Input.GetAxis("Horizontal"), 0.0f, 0.0f);
        moveVelocity = moveInput * speed;

       // Vector3 jumpInput = new Vector3(0.0f, Input.GetAxis("Vertical"), 0.0f);
       // jumpVelocity = jumpInput * jumpSpeed;

        if(isGrounded)
        {
            if(Input.GetKeyDown(KeyCode.Space))
            {
                rb.velocity = new Vector3(0f, 10f, 0f);
                isGrounded = false;
            }
        }

        //this give a negative y accelleration to the player making him to "go down" faster (as it increase the gravity) when it is not grounded
        
        /*if (isGrounded == false)
        {
                rb.AddForce(0f, -10f, 0f, ForceMode.Acceleration);
        }*/
           
    }

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.CompareTag("Ground"))
        {
            isGrounded = true;
        }
    }
    
    //This change the status "grounded" to "false" each time the player stop touching the ground
    //before it did that only when space was pressed (allowing to do at least a jump id leaving a platform without jumping before

    /*private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            isGrounded = false;
        }
    }
    */
    private void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveVelocity * Time.deltaTime);
        rb.MovePosition(rb.position + jumpVelocity * Time.deltaTime);
    }

   

    

}
